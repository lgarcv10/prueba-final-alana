/**
 * Class for Company Request
 */

export class CompanyRequest{

    public id: number;
    public userLastName: string;
    public userName: string;
    public locationName: string;
    public addressName: string;
    public addressLatitude: string;
    public addressLongitude: string;
    public tradeName: string;
    public statusType: string;
    public statusStatus: string;
    
}