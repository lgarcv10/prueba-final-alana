/**
 * Class for Franchise
 */

export class Franchise {

    public id: number;
    public franchiseName: string;
    public franchiseLogo: File;

}