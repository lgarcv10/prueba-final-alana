/**
 * Class for Job Technical Knowledge
 */

export class TechnicalKnowledge {

    public id: number;
    public name: string;

}