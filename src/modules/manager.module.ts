//Libraries
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule, Response, RequestOptions, Headers } from '@angular/http'; 
import { Observable }     from 'rxjs/Observable';
import { Ng2UploaderModule } from 'ng2-uploader';

//Components
import { ManagerLoginComponent } from '../pages/manager-pages/manager-log-in/manager-log-in.component';
import { ManagerLoggedComponent } from '../pages/manager-pages/manager-logged/manager-logged.component';
import { ManagerDashboardComponent } from '../pages/manager-pages/manager-dashboard/manager-dashboard.component';
import { ManagerRoutingModule } from '../routers/manager-routing-module';
import { FranchiseComponent } from '../pages/list-pages/franchise/franchise.component';
import { JobPositionComponent } from '../pages/list-pages/job-position/job-position.component';
import { TechnicalKnowledgeComponent } from '../pages/list-pages/technical-knowledge/technical-knowledge.component';
import { ViewJobPositionComponent } from '../pages/list-pages/job-position/view-job-position.component';
import { ViewTechnicalKnowledgeComponent } from '../pages/list-pages/technical-knowledge/view-technical-knowledge.component';
import { ViewFranchiseComponent } from '../pages/list-pages/franchise/view-franchise.component';
import { CreateJobPositionComponent } from '../pages/list-pages/job-position/create-job-position.component';
import { CreateFranchiseComponent } from '../pages/list-pages/franchise/create-franchise.component';
import { CompanyRequestComponent } from '../pages/list-pages/company-request/company-request.component';

//Services
import { AuthenticationService } from '../services/authentication.service';
import { JobService } from '../services/job.service';
import { FranchiseService } from '../services/franchise.service';
import { TechnicalKnowledgeService } from '../services/technical-knowledge.service';
import { CompanyRequestService } from '../services/company-request.service';



@NgModule({
  imports: [
    CommonModule,
    ManagerRoutingModule,
    FormsModule,
    HttpModule,
    Ng2UploaderModule 
  ],
  declarations: [
    ManagerLoginComponent,
    ManagerLoggedComponent,
    ManagerDashboardComponent,
    FranchiseComponent,
    JobPositionComponent,
    TechnicalKnowledgeComponent,
    ViewJobPositionComponent,
    ViewTechnicalKnowledgeComponent,
    ViewFranchiseComponent,
    CreateJobPositionComponent,
    CreateFranchiseComponent,
    CompanyRequestComponent
  ],
  providers: [
    AuthenticationService,
    JobService,
    FranchiseService,
    TechnicalKnowledgeService,
    CompanyRequestService
  ],
  exports: [
    ManagerLoginComponent,
    ManagerLoggedComponent,
    ManagerDashboardComponent,
    RouterModule,
    FranchiseComponent,
    JobPositionComponent,
    TechnicalKnowledgeComponent,
    ViewJobPositionComponent,
    ViewTechnicalKnowledgeComponent,
    ViewFranchiseComponent,
    CreateJobPositionComponent,
    CreateFranchiseComponent,
    CompanyRequestComponent
  ]
})
export class ManagerModule {}