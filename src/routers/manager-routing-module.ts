// app.routing.ts
import { NgModule } from '@angular/core';  
import { Routes, RouterModule } from '@angular/router';  

import { ManagerLoginComponent } from '../pages/manager-pages/manager-log-in/manager-log-in.component';
import { ManagerLoggedComponent } from '../pages/manager-pages/manager-logged/manager-logged.component';
import { ManagerDashboardComponent } from '../pages/manager-pages/manager-dashboard/manager-dashboard.component';
import { FranchiseComponent } from '../pages/list-pages/franchise/franchise.component';
import { JobPositionComponent } from '../pages/list-pages/job-position/job-position.component';
import { TechnicalKnowledgeComponent } from '../pages/list-pages/technical-knowledge/technical-knowledge.component';
import { ViewJobPositionComponent } from '../pages/list-pages/job-position/view-job-position.component';
import { ViewTechnicalKnowledgeComponent } from '../pages/list-pages/technical-knowledge/view-technical-knowledge.component';
import { ViewFranchiseComponent } from '../pages/list-pages/franchise/view-franchise.component';
import { CreateJobPositionComponent } from '../pages/list-pages/job-position/create-job-position.component';
import { CreateFranchiseComponent } from '../pages/list-pages/franchise/create-franchise.component';
import { CompanyRequestComponent } from '../pages/list-pages/company-request/company-request.component';

const MANAGER_ROUTES: Routes = [  
  { path: 'login', component: ManagerLoginComponent },
  { path: 'logged', component: ManagerLoggedComponent },
  { path: 'dashboard', component: ManagerDashboardComponent },
  { path: 'job', component: JobPositionComponent },
  { path: 'technical', component: TechnicalKnowledgeComponent },
  { path: 'franchise', component: FranchiseComponent },
  { path: 'view-job/:id', component: ViewJobPositionComponent },
  { path: 'view-knowledge/:id', component: ViewTechnicalKnowledgeComponent },
  { path: 'view-franchise/:id', component: ViewFranchiseComponent },
  { path: 'create-job', component: CreateJobPositionComponent },
  { path: 'create-franchise', component: CreateFranchiseComponent },
  { path: 'company-request', component: CompanyRequestComponent },
  { path: '', redirectTo: '/login',  pathMatch: 'full'}
]; 

@NgModule({
  imports: [RouterModule.forRoot(MANAGER_ROUTES)],
  exports: [RouterModule],
})
export class ManagerRoutingModule { }

export const routingComponents = [ManagerLoginComponent, ManagerLoggedComponent, FranchiseComponent, JobPositionComponent, TechnicalKnowledgeComponent, ViewJobPositionComponent, ViewFranchiseComponent, CreateJobPositionComponent, CreateFranchiseComponent,CompanyRequestComponent]; 