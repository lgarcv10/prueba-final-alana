/**
 * util class for endpoints
 */

export class Path {

    public static ENDPOINT: string = 'https://apidev.alanajobs.com';
    public static MANAGER_LOG_IN_ENDPOINT: string = '/secure-admin/login_check';
    public static GET_KNOWLEDGE_ENDPOINT: string = '/technical-knowledge/index';
    public static GET_POSITION_ENDPOINT: string = '/secure-admin/job-position/index';
    public static GET_FRANCHISE_ENDPOINT: string = '/secure-admin/franchise/index';
    public static GET_COMPANY_REQUEST_ENDPOINT: string = '/secure-admin/company-request/index';
    public static CREATE_POSITION_ENDPOINT: string = '/secure-admin/job-position/create';
    public static CREATE_FRANCHISE_ENDPOINT: string = '/secure-admin/franchise/create';
    public static APPROVE_COMPANY_REQUEST_ENDPOINT: string = '/secure-admin/company-request/approve';
    public static DISAPPROVE_COMPANY_REQUEST_ENDPOINT: string = '/secure-admin/company-request/disapprove';
}