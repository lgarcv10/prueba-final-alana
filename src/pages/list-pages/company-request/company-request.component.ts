import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { CompanyRequestService } from '../../../services/company-request.service';
import { CompanyRequest } from '../../../classes/CompanyRequest';

@Component({
  selector: 'company-request-component',
  templateUrl: './company-request.component.html',
  styleUrls: ['./company-request.component.css'],
  providers: [ CompanyRequestService ]
  
})

export class CompanyRequestComponent { 

  public companyRequests: CompanyRequest[];
  public companyRequest: CompanyRequest;
  public companyRequestId: number;
  public companyRequestName: string;
  public companyRequestLocation: string;
  public companyRequestStatus: string;
  
  constructor (private companyRequestService: CompanyRequestService, private http: Http){
        this.getRequests();
  }

  /**
   * Method to get a JSON with all company requests
   */
  public getRequests(): void {

        this.companyRequestService.getRequests().subscribe(
            companyRequests => {
            this.companyRequests = companyRequests;
            console.log(companyRequests);
            }, (error) => {
            console.log(error);
          });
    }

    
     /**
     * Method that find a company request by id
     */
    public getRequest(id: number){

      this.companyRequestService.getRequest(id).subscribe(
            companyRequest => {
            this.companyRequest = companyRequest;
            console.log(companyRequest);
            this.companyRequestId = this.companyRequest.id;
            this.companyRequestName = this.companyRequest.userLastName;
            this.companyRequestLocation = this.companyRequest.locationName;
            }, (error) => {
            console.log(error);
          });
    }

    /**
     * Method that update the status of the company request to approved
     * @param id 
     */
    public approveRequest(id:number){
            this.companyRequestService.approveRequest(id).subscribe(
            success => {
              alert("Solicitud aprobada exitosamente");
            }, error => {
              console.log(error);
           });
    }

    /**
     * Method that update the status of the company request to disapproved
     * @param id 
     */
    public disapproveRequest(id:number){
            this.companyRequestService.disapproveRequest(id).subscribe(
            success => {
              alert("Solicitud rechazada exitosamente");
            }, error => {
              console.log(error);
           });
    }

    public approveConfirmMessage(id: number)
    {
        var c = confirm("Estás completamente seguro de querer aprobar la solicitud?");

        if (c == true)
        {     
            this.approveRequest(id);
        }
        else
        {
            alert("No se aprobó la solicitud");
        }
 
    }

    public disapproveConfirmMessage(id: number)
    {
        var c = confirm("Estás completamente seguro de querer rechazar la solicitud?");

        if (c == true)
        {     
            this.disapproveRequest(id);
        }
        else
        {
            alert("No se rechazó la solicitud");
        }
 
    }

    




}
