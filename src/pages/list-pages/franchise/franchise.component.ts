import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { FranchiseService } from '../../../services/franchise.service';
import { Franchise } from '../../../classes/Franchise';

@Component({
  selector: 'franchise-component',
  templateUrl: './franchise.component.html',
  styleUrls: ['./franchise.component.css']
  
})

export class FranchiseComponent { 


    public franchises: Franchise[];
    public franchise: Franchise;

  constructor (private franchiseService: FranchiseService, private http: Http){
        this.getFranchises();
  }

  /**
   * Methos to get all franchises
   */
  public getFranchises(): void {

        this.franchiseService.getFranchises().subscribe(
            franchises => {
            this.franchises = franchises;
            console.log(franchises);

            }, (error) => {
            console.log(error);
            });
    }



}
