import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

//Services
import { FranchiseService } from '../../../services/franchise.service';
import { Franchise } from '../../../classes/Franchise';

@Component({
  selector: 'view-franchise-component',
  templateUrl: './view-franchise.component.html',
  styleUrls: ['./view-franchise.component.css'],
  providers: [ FranchiseService ]
  
})

export class ViewFranchiseComponent { 

  public franchise: Franchise;
  public franchises: Franchise[];
  public franchiseId: number;
  public franchiseName: string;
  public franchiseLogo: string;

  
  constructor (private franchiseService: FranchiseService, private http: Http, private _route: ActivatedRoute){
          this.franchiseId = parseInt(this._route.snapshot.params['id']);
          this.getFranchise(this.franchiseId);

  }


     /**
     * Method that find a franchise by id
     */
    public getFranchise(id: number){

      this.franchiseService.getFranchise(id).subscribe(
            franchise => {
            this.franchise = franchise;
            console.log('FRANQUICIA:');
            console.log(franchise);
            this.franchiseId = this.franchise.id;
            this.franchiseName = this.franchise.franchiseName;
            this.franchiseLogo = this.franchise.franchiseLogo.toString();
            }, (error) => {
            console.log(error);
          });
    }


}
