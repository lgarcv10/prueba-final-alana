import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

//Services
import { FranchiseService } from '../../../services/franchise.service';
import { Franchise } from '../../../classes/Franchise';

@Component({
  selector: 'create-franchise-component',
  templateUrl: './create-franchise.component.html',
  styleUrls: ['./create-franchise.component.css'],
  providers: [ FranchiseService ]
  
})

export class CreateFranchiseComponent { 

  public franchise: Franchise;
  public franchises: Franchise[];
  public franchiseId: number;
  public franchiseName: string;
  public franchiseLogo: File;
  public create_success: string = 'NO';
  
  constructor (private franchiseService: FranchiseService, private http: Http, private _route: ActivatedRoute){
    
  }

    /**
     * Method to create a new franchise
     * @param name 
     * @param logo 
     */
    public createFranchise_(name: string, logo: File): void {
              
       this.franchiseService.createFranchise(name,logo).subscribe(
            success => {
              alert("Franquicia creada exitosamente");
               this.create_success = 'YES';
            }, error => {
           });
   }

   createFranchise(formulario: NgForm){
     let fileInput: any = document.getElementById("img");
     //takes the first img
     let file = fileInput.files[0];

     this.franchiseLogo = file;
     console.log('DATOS DE FRANCHISE:');
     console.log(this.franchiseName);
     console.log(this.franchiseLogo);

     
     this.createFranchise_(this.franchiseName, this.franchiseLogo);

   }







}
