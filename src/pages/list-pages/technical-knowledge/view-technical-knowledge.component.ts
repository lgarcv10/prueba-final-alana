import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

//Services
import { TechnicalKnowledgeService } from '../../../services/technical-knowledge.service';
import { TechnicalKnowledge } from '../../../classes/TechnicalKnowledge';

@Component({
  selector: 'view-technical-knowledge-component',
  templateUrl: './view-technical-knowledge.component.html',
  styleUrls: ['./view-technical-knowledge.component.css'],
  providers: [ TechnicalKnowledgeService ]
  
})

export class ViewTechnicalKnowledgeComponent { 

  public technicalKnowledge: TechnicalKnowledge;
  public technicalKnowledges: TechnicalKnowledge[];
  public technicalKnowledgeId: number;
  public technicalKnowledgeName: string;
  
  constructor (private technicalKnowledgeService: TechnicalKnowledgeService, private http: Http, private _route: ActivatedRoute){
          this.technicalKnowledgeId = parseInt(this._route.snapshot.params['id']);
          this.getTechnicalKnowledge(this.technicalKnowledgeId);

  }


     /**
     * Method that find a technical knowledge by id
     */
    public getTechnicalKnowledge(id: number){

      this.technicalKnowledgeService.getTechnicalKnowledge(id).subscribe(
            technicalKnowledge => {
            this.technicalKnowledge = technicalKnowledge;
            console.log('Conocimiento:');
            console.log(technicalKnowledge);
            this.technicalKnowledgeId = this.technicalKnowledge.id;
            this.technicalKnowledgeName = this.technicalKnowledge.name;
            }, (error) => {
            console.log(error);
          });
    }


}
