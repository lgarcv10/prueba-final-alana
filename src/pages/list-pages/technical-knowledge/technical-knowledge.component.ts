import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { TechnicalKnowledgeService } from '../../../services/technical-knowledge.service';
import { TechnicalKnowledge } from '../../../classes/TechnicalKnowledge';

@Component({
  selector: 'technical-knowledge-component',
  templateUrl: './technical-knowledge.component.html',
  styleUrls: ['./technical-knowledge.component.css']
  
})

export class TechnicalKnowledgeComponent { 

    public technicalKnowledges: TechnicalKnowledge[];
    public TechnicalKnowledge: TechnicalKnowledge;

  constructor (private technicalKnowledgeService: TechnicalKnowledgeService, private http: Http){
        this.getTechnicalKnowledges();
  }

  /**
   * Method that get all technical knowledges
   */

  public getTechnicalKnowledges(): void {

        this.technicalKnowledgeService.getTechnicalKnowledges().subscribe(
            technicalKnowledges => {
            this.technicalKnowledges = technicalKnowledges;
            console.log(technicalKnowledges);
          }, 
          
            (error) => {
            console.log(error);
            });
    }



}
