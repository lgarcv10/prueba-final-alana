import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { JobService } from '../../../services/job.service';
import { JobPosition } from '../../../classes/JobPosition';

@Component({
  selector: 'job-position-component',
  templateUrl: './job-position.component.html',
  styleUrls: ['./job-position.component.css'],
  providers: [ JobService ]
  
})

export class JobPositionComponent { 

  public positions: JobPosition[];
  public position: Position;
  public positionId: number;
  
  constructor (private jobService: JobService, private http: Http){
        this.getPositions();
  }

  /**
   * Method to get a JSON with all positions
   */
  public getPositions(): void {

        this.jobService.getPositions().subscribe(
            positions => {
            this.positions = positions;
            console.log(positions);
            }, (error) => {
            console.log(error);
          });
    }


}
