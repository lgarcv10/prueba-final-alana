import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

//Services
import { JobService } from '../../../services/job.service';
import { JobPosition } from '../../../classes/JobPosition';

@Component({
  selector: 'create-job-position-component',
  templateUrl: './create-job-position.component.html',
  styleUrls: ['./create-job-position.component.css'],
  providers: [ JobService ]
  
})

export class CreateJobPositionComponent { 

  public position: JobPosition;
  public positions: JobPosition[];
  public positionId: number;
  public positionName: string;
  
  constructor (private jobService: JobService, private http: Http, private _route: ActivatedRoute){
    
  }

  

}
