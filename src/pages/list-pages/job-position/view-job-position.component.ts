import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute } from '@angular/router';

//Services
import { JobService } from '../../../services/job.service';
import { JobPosition } from '../../../classes/JobPosition';

@Component({
  selector: 'view-job-position-component',
  templateUrl: './view-job-position.component.html',
  styleUrls: ['./view-job-position.component.css'],
  providers: [ JobService ]
  
})

export class ViewJobPositionComponent { 

  public position: JobPosition;
  public positions: JobPosition[];
  public positionId: number;
  public positionName: string;
  
  constructor (private jobService: JobService, private http: Http, private _route: ActivatedRoute){
          this.positionId = parseInt(this._route.snapshot.params['id']);
          this.getPosition(this.positionId);

  }


     /**
     * Method that find a position by id
     */
    public getPosition(id: number){

      this.jobService.getPosition(id).subscribe(
            position => {
            this.position = position;
            console.log(position);
            this.positionId = this.position.id;
            this.positionName = this.position.jobPositionName;
            }, (error) => {
            console.log(error);
          });
    }


}
