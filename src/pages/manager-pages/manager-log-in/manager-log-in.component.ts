import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { User } from '../../../classes/User';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'manager-log-in-component',
  templateUrl: './manager-log-in.component.html',
  styleUrls: ['./manager-log-in.component.css']
  
})

export class ManagerLoginComponent { 


  public email: string = 'danielbernardez7@gmail.com';
  public password: string = '1mpr3ss2017';
  public profileId: string;
  public user: User= new User('','','');

  constructor (private authenticationService: AuthenticationService){

  }
  /**
   * Method to sign in as a manager
   */
  public signIn(): void {
      if (this.email != '' && this.password != '') {

        this.user.email = this.email;
        this.user.password = this.password;
          this.authenticationService.SignIn(this.user).subscribe(
              success => {
                
              },
              err => {
                console.log(err);
              });
      }
  }

}
