import { Component, Injectable, OnInit } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { JobPosition } from '../classes/JobPosition';
import { Path } from '../utils/Path';

@Injectable()
export class JobService {
    
    public requestURL: string;
    public endPoint: string;

    constructor(private http: Http){

    }

    /**
     * Method that get all positions
     */
    public getPositions(): Observable<any> {

        return this.http.get(Path.ENDPOINT + Path.GET_POSITION_ENDPOINT, this.jwt())
        .map(res => res.json()['response']);
             
    };

    /**
     * Method that returns one position, searching by id
     * @param id 
     */
    getPosition(id: number): Observable<JobPosition> { 
        return this.getPositions()
        .map(position => position.find(position => position.id === id));
        
    }

    private jwt(): RequestOptions {
        // create authorization header with jwt token
        let currentManger = JSON.parse(localStorage.getItem('currentUser'));
        if (currentManger && currentManger.token) {
            let _headers = new Headers({ 'Authorization': 'Bearer ' + currentManger.token });
            return new RequestOptions({ headers: _headers });
        }
    }

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 


}