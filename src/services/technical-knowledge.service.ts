import { Component, Injectable, OnInit } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { TechnicalKnowledge } from '../classes/TechnicalKnowledge';

import { Path } from '../utils/Path';

@Injectable()
export class TechnicalKnowledgeService {
    
    public requestURL: string;
    public endPoint: string;

    constructor(private http: Http){
    }

    /**
     * Method that get all technical knowledges
     */
    public getTechnicalKnowledges(): Observable<any> {
        
        return this.http.get(Path.ENDPOINT + Path.GET_KNOWLEDGE_ENDPOINT, this.jwt)
        .map(res => res.json()['response']); 
             
    };

    /**
     * Method that returns one technical knowledge, searching by id
     * @param id 
     */
    getTechnicalKnowledge(id: number): Observable<TechnicalKnowledge> { 
        return this.getTechnicalKnowledges()
        .map(technicalKnowledge => technicalKnowledge.find(technicalKnowledge => technicalKnowledge.id === id));
        
    }


    private jwt(): RequestOptions {
        // create authorization header with jwt token
        console.log('CURRENT JWT ====> '+localStorage.getItem('currentUser'));
        let currentManger = JSON.parse(localStorage.getItem('currentUser'));
        if (currentManger && currentManger.authToken) {
            let _headers = new Headers({ 'Authorization': 'Bearer ' + currentManger.authToken });
            return new RequestOptions({ headers: _headers });
        }
    }

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 


}