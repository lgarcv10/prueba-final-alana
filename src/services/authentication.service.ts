import { Injectable } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from '../classes/User';

import { Path } from '../utils/Path';

@Injectable()
export class AuthenticationService {
    
    public requestURL: string;

    constructor(public http: Http){
 
    }

    //Method to Sign in users list
    public SignIn(usuario: User): Observable<any> {
        
        let body: string = 'email='+ usuario.email + '&password=' + usuario.password;

        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: headers });
        
        return this.http.post(Path.ENDPOINT + Path.MANAGER_LOG_IN_ENDPOINT, body, {headers: headers})
            .map((response: Response) => {
                console.log(response);
                response.json();
                usuario.token = JSON.parse(response['_body']).token;
                localStorage.setItem('currentUser', JSON.stringify(usuario));
                localStorage.getItem('currentUser');
                
            })
            .catch(this.handleErrorObservable);
    };

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 




}