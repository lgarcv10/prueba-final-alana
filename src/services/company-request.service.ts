import { Component, Injectable, OnInit } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { CompanyRequest } from '../classes/CompanyRequest';

import { Path } from '../utils/Path';

@Injectable()
export class CompanyRequestService {

    constructor(private http: Http){
        
    }

    /**
     * Method that get all company requests
     */
    public getRequests(): Observable<any> {

        return this.http.get(Path.ENDPOINT + Path.GET_COMPANY_REQUEST_ENDPOINT, this.jwt())
        .map(res => res.json()['response']);
             
    };


    /**
     * Method that returns one company request, searching by id
     * @param id 
     */
    getRequest(id: number): Observable<CompanyRequest> { 
        return this.getRequests()
        .map(companyRequest => companyRequest.find(companyRequest => companyRequest.id === id));
        
    }
    
    /**
     * Method that creates a new franchise
     * @param name 
     * @param logo 
     */
    public createFranchise(name, logo){

       const formData = new FormData();
       formData.append('name', name);
       formData.append('logo', logo);

        return this.http.post(Path.ENDPOINT + Path.CREATE_FRANCHISE_ENDPOINT, formData, this.jwt())
           .map((response: Response) => response.json())
           .catch((error: any) => Observable.throw(error.json()));
   }

   /**
     * approve a company request by id
     */
   public approveRequest(id: number): Observable<CompanyRequest>{
    return this.http.post(Path.ENDPOINT+Path.APPROVE_COMPANY_REQUEST_ENDPOINT, {id: + id}, this.jwt())
    .map((response: Response) => response.json());
   }

      /**
     * disapprove a company request by id
     */
   public disapproveRequest(id: number): Observable<CompanyRequest>{
    return this.http.post(Path.ENDPOINT+Path.DISAPPROVE_COMPANY_REQUEST_ENDPOINT, {id: + id}, this.jwt())
    .map((response: Response) => response.json());
   }

    /**
     * create authorization header with jwt token
     */
    private jwt(): RequestOptions {
        let currentManger = JSON.parse(localStorage.getItem('currentUser'));
        if (currentManger && currentManger.token) {
            let _headers = new Headers({ 'Authorization': 'Bearer ' + currentManger.token });
            return new RequestOptions({ headers: _headers });
        }
    }

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 


}