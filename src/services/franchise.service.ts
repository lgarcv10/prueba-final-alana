import { Component, Injectable, OnInit } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Franchise } from '../classes/Franchise';

import { Path } from '../utils/Path';

@Injectable()
export class FranchiseService {
    
    public requestURL: string;
    public endPoint: string;

    constructor(private http: Http){
        
    }


    /**
     * Method that get all franchises
     */
    public getFranchises(): Observable<any> {

        return this.http.get(Path.ENDPOINT + Path.GET_FRANCHISE_ENDPOINT, this.jwt())
        .map(res => res.json()['response']);
             
    };


    /**
     * Method that returns one franchise, searching by id
     * @param id 
     */
    getFranchise(id: number): Observable<Franchise> { 
        return this.getFranchises()
        .map(franchise => franchise.find(franchise => franchise.id === id));
        
    }

    /**
     * Method that creates a new franchise
     * @param name 
     * @param logo 
     */
    public createFranchise(name, logo){

       const formData = new FormData();
       formData.append('name', name);
       formData.append('logo', logo);

        return this.http.post(Path.ENDPOINT + Path.CREATE_FRANCHISE_ENDPOINT, formData, this.jwt())
           .map((response: Response) => response.json())
           .catch((error: any) => Observable.throw(error.json()));
   }

    /**
     * create authorization header with jwt token
     */
    private jwt(): RequestOptions {
        let currentManger = JSON.parse(localStorage.getItem('currentUser'));
        if (currentManger && currentManger.token) {
            let _headers = new Headers({ 'Authorization': 'Bearer ' + currentManger.token });
            return new RequestOptions({ headers: _headers });
        }
    }

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 


}